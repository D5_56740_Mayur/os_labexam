#!/bin/bash

# script to implement menu driven program to perform given commands
while [ 1 ]
do
    echo "1.Armstrong numbers in given range :"
    echo "2.Print star pattern :"
    echo "3.Addition of floating point numbers  :"
    echo "4.Add execute permissions to given file :"
    echo "5.Convert upper case to lower case :"
    echo "6.Find file with name in home directory :"
    echo "7.Find number of occurances of a word in a file :"
    echo "8.Exit"
    echo "Enter choice"
    read choice

     case $choice in
    1)
        echo "Enter lower limit : "
        read lower
        echo "Enter upper limit : "
        read upper

        for (( i=lower;i<upper;i++ ))
        do  
            sumofdigits=0
            num=$i
            while [ $num -gt 0 ]
            do
                mod=`expr $num % 10`
                cube=`expr $mod \* $mod \* $mod`
                sumofdigits=`expr $sumofdigits + $cube`
                num=`expr $num / 10`
            done
            
            if [ $sumofdigits -eq $i ]
            then
                echo "$i is an armstrong number "            
            fi
        done 
        ;;

         2)
        echo "Enter number of rows : "
        read num
        for (( i=1;i<=num;i++))
        do
            for ((j=1;j<=num - 1;j++))
            do
                echo -n "  "
            done
            
            for ((j=1;j<=2 * i -1;j++))
            do
                echo -n "* "
            done
            echo
        done
        ;;

        
    3)
        echo -n "Enter number1 : " 
        read num1
        echo -n "Enter number2 : "
        read num2

        echo -n "Addition of the numbers is : "
        c=`echo $num1+$num2 | bc`
        echo "$c"
        ;;

        
    4)
        echo "Enter filpath : "
        read filepath
        if [ -e $filepath ]
        then
            if [ -f $filepath ]
            then
                chmod +x "$filepath" 
                 echo -n " Permission Provided "

            fi
        fi
        ;;
    5)
        echo -n "Enter a string having upper case letters : "
        read string

        echo $string | tr '[:upper:]' '[:lower:]'
        ;;

    6)
        echo -n "Enter name of the file to search it : "
        read name
        cd ~ 
        find $name 
        ;; 

    7)
        echo -n " Enter file name : "
        read name1
        echo -n  " Enter word to count occurances of : "
        read word
        grep -o "$word" "$name1" | wc -w 
        ;;    
    8)
        exit
        ;;
    esac
    
done


# Write a menu driven shell script with following functionalities:

# 1. Print all Armstrong numbers between given range. (Provide range as positional parameters)

# (Armstrong number => Original number == sum of its digits raised to the third power (to calculate third power, don't use any linux command))

# 2. Pring pattern. (Take number of rows from user)

# 	    *

# 	   * *

# 	  * * *

# 	 * * * *

# 	* * * * *

# 3. Command 1 : Write a command to print addition of two floating point numbers.

# 4. Command 2 : Write a command to add execute permissions for user of given file.

# 5. Command 3 : Write a command to convert upper case letters of given string to lower case.

# 6. Command 4 : Write a command to find file with name in your home directory.

# 7. Command 5 : Write a command to count number of occurrences of a word in file.



